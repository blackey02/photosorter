PhotoSorter
====================

Summary
---------------------
I was looking for a way to automatically sort all of my pictures by date and also by geo location if available, and lets face it all photos are taken by smart phones these days so it's usually available. After finding a couple solutions that required payment to get the features I wanted I decided to write my own with the help of Exiflib.

Features
---------------------
* Sorts photos by date and location
* Photos are first sorted in ascending order so when traversing pictures they are in chronological order
* Files with no exif data are placed in the "Unknown" folder
* KML files are produced so you can see which cities you've been in

Building
---------------------
0. Get a [Bing Maps API key](http://msdn.microsoft.com/en-us/library/ff428642.aspx)
1. git clone https://bitbucket.org/blackey02/photosorter.git
2. Open 'PhotoSorter\App.config' and place your Bing Maps API key in the 'value' element.
```xml
<PhotoSorter.Properties.Settings>
		<setting name="BingMapsKey" serializeAs="String">
			<value /> // Your bing key goes here
		</setting>
		<setting name="DismissDuplicates" serializeAs="String">
			<value>True</value> // True to ignore duplicate files
		</setting>
		<setting name="FindPhotosThreads" serializeAs="String">
			<value>2</value> // 2 is a good balance
		</setting>
		<setting name="CopyPhotosThreads" serializeAs="String">
			<value>2</value> // 2 is a good balance
		</setting>
		<setting name="LookupGpsAddress" serializeAs="String">
			<value>True</value> // True to use the Bing key to look up physical address from GPS coords
		</setting>
		<setting name="LookupGpsAddressRetryCount" serializeAs="String">
			<value>20</value> // How many times to retry the Bing request if it fails for some reason
		</setting>
		<setting name="GenerateKml" serializeAs="String">
			<value>True</value> // Do you want to generate a kml document?
		</setting>
		<setting name="KmlCityDistinct" serializeAs="String">
			<value>True</value> // When generating a kml document, true to include only distinct city names
		</setting>
		<setting name="CopyPhotos" serializeAs="String">
			<value>True</value> // True to copy photos to the destination folder, False can be used if testing or only generating a kml
		</setting>
		<setting name="KmlSplitCount" serializeAs="String">
			<value>0</value> // If for some reason you want to split the kml files on a certain number of locations, you can specify that here
		</setting>
		<setting name="ExistingKmlPath" serializeAs="String">
			<value /> // If running this application for a second time you may have an existing kml that you would like to append new locations to, you just need to put in the path to your existing kml here. e.g. c:\Data.kml
		</setting>
	</PhotoSorter.Properties.Settings>
</userSettings>
```
3. Open PhotoSorter.sln with Visual Studio 2010
4. Build and run

Screens
---------------------
![](https://copy.com/rAwo0PjDT8pj)
![](https://copy.com/breeVzSYxED6)
![](https://copy.com/pzDC7m02yAkp)
![](https://copy.com/A0Z8KZipe1AS)

Acknowledgements
---------------------
* [Exiflib](http://www.codeproject.com/Articles/36342/ExifLib-A-Fast-Exif-Data-Extractor-for-NET)