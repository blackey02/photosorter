﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace PhotoSorter
{
    public partial class PhotoSorterForm : Form
    {
        public PhotoSorterForm()
        {
            InitializeComponent();
            Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.InvariantCulture;
        }

        private void buttonIn_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialogIn.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string pathIn = folderBrowserDialogIn.SelectedPath;
                textBoxIn.Text = pathIn;
            }
        }

        private void buttonOut_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialogOut.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string pathOut = folderBrowserDialogOut.SelectedPath;
                textBoxOut.Text = pathOut;
            }
        }

        private void buttonGo_Click(object sender, EventArgs e)
        {
            var pathIn = textBoxIn.Text;
            var pathOut = textBoxOut.Text;

            if (!string.IsNullOrWhiteSpace(pathIn) && !string.IsNullOrWhiteSpace(pathOut))
            {
                progressBarFound.Value = progressBarFound.Minimum;
                progressBarCopied.Value = progressBarCopied.Minimum;
                progressBarKml.Value = progressBarKml.Minimum;

                ThreadPool.QueueUserWorkItem(obj =>
                    {
                        Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.InvariantCulture;
                        DirUtils.Instance.PhotoFoundEvent += new Action<object, float, string>(PhotoFoundEvent);
                        DirUtils.Instance.PhotoCopiedEvent += new Action<object, float, string>(PhotoCopiedEvent);
                        var photosFound = DirUtils.Instance.FindPhotos(pathIn);
                        if(Properties.Settings.Default.CopyPhotos)
                            DirUtils.Instance.CopyPhotos(pathOut, photosFound);
                        var mapper = new Mapper(photosFound);
                        mapper.PhotoProcessedEvent += new Action<object,float,string>(PhotoProcessedEvent);
                        if(Properties.Settings.Default.GenerateKml)
                            mapper.GenerateMaps(pathOut);
                        mapper.PhotoProcessedEvent -= new Action<object, float, string>(PhotoProcessedEvent);
                        DirUtils.Instance.PhotoFoundEvent -= new Action<object, float, string>(PhotoFoundEvent);
                        DirUtils.Instance.PhotoCopiedEvent -= new Action<object, float, string>(PhotoCopiedEvent);
                    });
            }
        }

        private void PhotoProcessedEvent(object sender, float pctDone, string fileName)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new Action<object, float, string>(PhotoProcessedEvent), sender, pctDone, fileName);
                return;
            }
            labelKml.Text = fileName;
            progressBarKml.Value = (int)(pctDone * progressBarFound.Maximum);
        }

        private void PhotoFoundEvent(object sender, float pctDone, string fileName)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new Action<object, float, string>(PhotoFoundEvent), sender, pctDone, fileName);
                return;
            }
            labelFound.Text = fileName;
            progressBarFound.Value = (int)(pctDone * progressBarFound.Maximum);
        }

        private void PhotoCopiedEvent(object sender, float pctDone, string fileName)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new Action<object, float, string>(PhotoCopiedEvent), sender, pctDone, fileName);
                return;
            }
            labelCopied.Text = fileName;
            progressBarCopied.Value = (int)(pctDone * progressBarCopied.Maximum);
        }
    }
}
