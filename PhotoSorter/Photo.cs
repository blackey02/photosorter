﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Security.Cryptography;
using System.IO;

namespace PhotoSorter
{
    class Photo
    {
        private string _ext;
        private static int StaticCounter = 0;

        public string Path { get; private set; }
        public ExifReader ExifData { get; private set; }
        public string Checksum { get; private set; }

        public Photo(string file)
        {
            Path = file.Trim();

            if (Properties.Settings.Default.DismissDuplicates)
            {
                using (var md5 = MD5.Create())
                {
                    Checksum = BitConverter.ToString(md5.ComputeHash(File.ReadAllBytes(Path)));
                }
            }
            else
                Checksum = Path;

            _ext = System.IO.Path.GetExtension(Path).ToLower();
            ExifData = new ExifReader(Path);
            ExifData.Parse();
        }

        public string SuggestedFilename(string append = "")
        {
            var name = string.Empty;

            if (ExifData.HasDate)
                name = ExifData.ExifDate.ToString("yyyyMMdd");
            else
                name = (Interlocked.Increment(ref StaticCounter)).ToString("0000");
            
            if (string.IsNullOrWhiteSpace(append))
                append = string.Empty;
            if (!string.IsNullOrWhiteSpace(append))
                append = string.Format("_{0}", append);
            name = string.Format("{0}{1}{2}", name, append, _ext);
            return name;
        }

        public override string ToString()
        {
            return Path;
        }
    }
}
