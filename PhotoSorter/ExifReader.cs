﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;
using System.Net;
using System.Globalization;
using System.Threading;
using System.Xml.Linq;

namespace PhotoSorter
{
    class ExifReader
    {
        private string _path;

        public DateTime ExifDate { get; private set; }
        public Location ExifLocation { get; set; }
        public double ExifLatitude { get; private set; }
        public double ExifLongitude { get; private set; }

        public ExifReader(string path)
        { 
            _path = path;
            ExifDate = DateTime.MaxValue;
            ExifLongitude = double.NaN;
            ExifLatitude = double.NaN;
        }

        public bool HasDate
        {
            get
            {
                return ExifDate != DateTime.MaxValue;
            }
        }

        public bool HasLocation
        {
            get
            {
                return ExifLocation != null;
            }
        }

        public bool HasGps
        {
            get
            {
                return !double.IsNaN(ExifLatitude) && !double.IsNaN(ExifLongitude);
            }
        }

        public void Parse()
        {
            try
            {
                using (var exif = new ExifLib.ExifReader(_path))
                {
                    // Get date/time
                    DateTime dateTime;
                    bool hasDateTime;
                    exif.GetTagValue(ExifLib.ExifTags.DateTimeDigitized, out dateTime);
                    hasDateTime = dateTime != default(DateTime);
                    if (hasDateTime)
                        ExifDate = DateTime.SpecifyKind(dateTime, DateTimeKind.Local);

                    // Get GPS
                    double[] latGps, longGps;
                    double latitude = 0, longitude = 0;
                    string latRef, longRef;
                    bool hasGps;
                    exif.GetTagValue(ExifLib.ExifTags.GPSLatitude, out latGps);
                    hasGps = latGps != null && latGps.Length == 3;
                    if (hasGps)
                    {
                        exif.GetTagValue(ExifLib.ExifTags.GPSLatitudeRef, out latRef);
                        exif.GetTagValue(ExifLib.ExifTags.GPSLongitude, out longGps);
                        exif.GetTagValue(ExifLib.ExifTags.GPSLongitudeRef, out longRef);
                        latitude = latGps[0] + latGps[1] / 60 + latGps[2] / 3600;
                        longitude = longGps[0] + longGps[1] / 60 + longGps[2] / 3600;

                        if (latRef.ToLower() == "s")
                            latitude *= -1;
                        if (longRef.ToLower() == "w")
                            longitude *= -1;

                        ExifLatitude = latitude;
                        ExifLongitude = longitude;

                        if (Properties.Settings.Default.LookupGpsAddress)
                        {
                            var uri = string.Format("http://dev.virtualearth.net/REST/v1/Locations/{0},{1}?o=xml&key={2}", ExifLatitude, ExifLongitude, Properties.Settings.Default.BingMapsKey);
                            using (var webClient = new WebClient())
                            {
                                int retry = Properties.Settings.Default.LookupGpsAddressRetryCount;
                                for (int i = 0; i < retry; i++)
                                {
                                    string xml;
                                    XDocument doc;
                                    XNamespace ns;
                                    IEnumerable<XElement> addresses;
                                    XElement address;

                                    try
                                    {
                                        xml = webClient.DownloadString(uri);
                                        doc = XDocument.Parse(xml);
                                        ns = doc.Root.Name.Namespace;

                                        addresses = doc.Descendants(ns + "Address");
                                        if (addresses.FirstOrDefault() == null)
                                        {
                                            Logger.Instance.Log(uri, string.Format("geo-retry ({0})", i + 1));
                                            continue;
                                        }

                                        address = addresses.FirstOrDefault();
                                        var city = address.Element(ns + "Locality");
                                        if (city == null)
                                            city = address.Element(ns + "AdminDistrict2");

                                        ExifLocation = new Location();
                                        ExifLocation.City = city.Value;
                                        ExifLocation.State = address.Element(ns + "AdminDistrict").Value;
                                        ExifLocation.Country = address.Element(ns + "CountryRegion").Value;
                                    }
                                    catch (Exception e)
                                    {
                                        Logger.Instance.Log(e);
                                        ExifLocation = null;
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Instance.Log(e);
                ExifDate = DateTime.MaxValue;
                ExifLocation = null;
            }
        }
    }
}
