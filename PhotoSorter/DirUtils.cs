﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;
using System.Collections.Concurrent;

namespace PhotoSorter
{
    class DirUtils
    {
        public event Action<object, float, string> PhotoFoundEvent;
        public event Action<object, float, string> PhotoCopiedEvent;
        private static DirUtils _instance = null;

        public static DirUtils Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new DirUtils();
                return _instance;
            }
        }

        private DirUtils()
        { }

        private void FirePhotoFoundEvent(float pctDone, string fileName)
        {
            if (PhotoFoundEvent != null)
                PhotoFoundEvent(this, pctDone, fileName);
        }

        public List<Photo> FindPhotos(string path)
        {
            var files = Directory.EnumerateFiles(path, "*.*", SearchOption.AllDirectories);
            files = files.Where(file => !(Path.GetFileName(file).StartsWith(".")));
            
            int totalFiles = files.Count();
            var photos = new List<Photo>();
            int count = 0;
            int maxThreads = Properties.Settings.Default.FindPhotosThreads;
            var semi = new Semaphore(maxThreads, maxThreads);

            foreach (string file in files)
            {
                string tmpFile = file;
                semi.WaitOne();
                ThreadPool.QueueUserWorkItem(obj =>
                    {
                        Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.InvariantCulture;
                        photos.Add(new Photo(tmpFile));
                        FirePhotoFoundEvent(Interlocked.Increment(ref count) * 1.0f / totalFiles, tmpFile);
                        semi.Release();
                    });
            }

            while (count < totalFiles)
                Thread.Sleep(20);

            var duplicates = photos.GroupBy(x => x.Checksum)
                .Where(g => g.Count() > 1)
                .ToDictionary(x => x.Key, y => y.ToList());

            foreach(string key in duplicates.Keys)
                Logger.Instance.Log(duplicates[key], "duplicate");

            var enumPhotos = photos.Distinct(new PhotoComparer());
            photos = enumPhotos.OrderBy(photo => photo.ExifData.ExifDate).ToList();
            return photos;
        }

        private void FirePhotoCopiedEvent(float pctDone, string fileName)
        {
            if (PhotoCopiedEvent != null)
                PhotoCopiedEvent(this, pctDone, fileName);
        }

        public void CopyPhotos(string path, List<Photo> photos)
        {
            var idxTracker = new ConcurrentDictionary<string, int>();
            int totalPhotos = photos.Count;
            int count = 0;
            var unknownDir = Path.Combine(path, "Unknown");
            int maxThreads = Properties.Settings.Default.CopyPhotosThreads;
            var semi = new Semaphore(maxThreads, maxThreads);

            foreach (Photo photo in photos)
            {
                Photo tmpPhoto = photo;
                semi.WaitOne();
                ThreadPool.QueueUserWorkItem(obj =>
                    {
                        Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.InvariantCulture;
                        var idSymbols = tmpPhoto.Path.Split('\\');
                        int secondToLast = 2, last = 1;
                        if (idSymbols.Length >= 3)
                        {
                            secondToLast = 3;
                            last = 2;
                        }

                        var idStr = string.Format("{0}_{1}", idSymbols[idSymbols.Length - secondToLast], idSymbols[idSymbols.Length - last]);
                        idStr = idStr.Replace(" ", "_");
                        string locationDir = Path.Combine(unknownDir, idStr);

                        if (tmpPhoto.ExifData.HasDate)
                        {
                            var yearDir = Path.Combine(path, tmpPhoto.ExifData.ExifDate.Year.ToString());
                            var monthDir = Path.Combine(yearDir, tmpPhoto.ExifData.ExifDate.ToString("MMMM"));
                            locationDir = monthDir;

                            if (tmpPhoto.ExifData.HasLocation)
                                locationDir = Path.Combine(monthDir, tmpPhoto.ExifData.ExifLocation.DirectoryFriendly);
                        }

                        int trackerIdx = 0;
                        lock (idxTracker)
                        {
                            if (!Directory.Exists(locationDir))
                                Directory.CreateDirectory(locationDir);
                            if (!idxTracker.ContainsKey(locationDir))
                                idxTracker.TryAdd(locationDir, 0);
                            else
                                idxTracker[locationDir]++;
                            trackerIdx = idxTracker[locationDir];
                        }

                        string newFilePath = tmpPhoto.SuggestedFilename(trackerIdx.ToString());
                        newFilePath = Path.Combine(locationDir, newFilePath);
                        File.Copy(tmpPhoto.Path, newFilePath, true);

                        FirePhotoCopiedEvent(Interlocked.Increment(ref count) * 1.0f / totalPhotos, newFilePath);
                        semi.Release();
                    });
            }

            while (count < totalPhotos)
                Thread.Sleep(20);
        }
    }
}
