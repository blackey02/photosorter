﻿namespace PhotoSorter
{
    partial class PhotoSorterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.folderBrowserDialogIn = new System.Windows.Forms.FolderBrowserDialog();
            this.buttonIn = new System.Windows.Forms.Button();
            this.groupBoxIn = new System.Windows.Forms.GroupBox();
            this.textBoxIn = new System.Windows.Forms.TextBox();
            this.groupBoxOut = new System.Windows.Forms.GroupBox();
            this.textBoxOut = new System.Windows.Forms.TextBox();
            this.buttonOut = new System.Windows.Forms.Button();
            this.buttonGo = new System.Windows.Forms.Button();
            this.folderBrowserDialogOut = new System.Windows.Forms.FolderBrowserDialog();
            this.progressBarFound = new System.Windows.Forms.ProgressBar();
            this.progressBarCopied = new System.Windows.Forms.ProgressBar();
            this.labelFound = new System.Windows.Forms.Label();
            this.labelCopied = new System.Windows.Forms.Label();
            this.labelKml = new System.Windows.Forms.Label();
            this.progressBarKml = new System.Windows.Forms.ProgressBar();
            this.groupBoxIn.SuspendLayout();
            this.groupBoxOut.SuspendLayout();
            this.SuspendLayout();
            // 
            // folderBrowserDialogIn
            // 
            this.folderBrowserDialogIn.ShowNewFolderButton = false;
            // 
            // buttonIn
            // 
            this.buttonIn.Location = new System.Drawing.Point(6, 29);
            this.buttonIn.Name = "buttonIn";
            this.buttonIn.Size = new System.Drawing.Size(47, 23);
            this.buttonIn.TabIndex = 0;
            this.buttonIn.Text = "...";
            this.buttonIn.UseVisualStyleBackColor = true;
            this.buttonIn.Click += new System.EventHandler(this.buttonIn_Click);
            // 
            // groupBoxIn
            // 
            this.groupBoxIn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxIn.Controls.Add(this.textBoxIn);
            this.groupBoxIn.Controls.Add(this.buttonIn);
            this.groupBoxIn.Location = new System.Drawing.Point(12, 12);
            this.groupBoxIn.Name = "groupBoxIn";
            this.groupBoxIn.Size = new System.Drawing.Size(425, 79);
            this.groupBoxIn.TabIndex = 1;
            this.groupBoxIn.TabStop = false;
            this.groupBoxIn.Text = "Input";
            // 
            // textBoxIn
            // 
            this.textBoxIn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxIn.Location = new System.Drawing.Point(59, 31);
            this.textBoxIn.Name = "textBoxIn";
            this.textBoxIn.ReadOnly = true;
            this.textBoxIn.Size = new System.Drawing.Size(360, 20);
            this.textBoxIn.TabIndex = 1;
            // 
            // groupBoxOut
            // 
            this.groupBoxOut.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxOut.Controls.Add(this.textBoxOut);
            this.groupBoxOut.Controls.Add(this.buttonOut);
            this.groupBoxOut.Location = new System.Drawing.Point(12, 102);
            this.groupBoxOut.Name = "groupBoxOut";
            this.groupBoxOut.Size = new System.Drawing.Size(425, 79);
            this.groupBoxOut.TabIndex = 2;
            this.groupBoxOut.TabStop = false;
            this.groupBoxOut.Text = "Output";
            // 
            // textBoxOut
            // 
            this.textBoxOut.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxOut.Location = new System.Drawing.Point(59, 31);
            this.textBoxOut.Name = "textBoxOut";
            this.textBoxOut.ReadOnly = true;
            this.textBoxOut.Size = new System.Drawing.Size(360, 20);
            this.textBoxOut.TabIndex = 1;
            // 
            // buttonOut
            // 
            this.buttonOut.Location = new System.Drawing.Point(6, 29);
            this.buttonOut.Name = "buttonOut";
            this.buttonOut.Size = new System.Drawing.Size(47, 23);
            this.buttonOut.TabIndex = 0;
            this.buttonOut.Text = "...";
            this.buttonOut.UseVisualStyleBackColor = true;
            this.buttonOut.Click += new System.EventHandler(this.buttonOut_Click);
            // 
            // buttonGo
            // 
            this.buttonGo.Location = new System.Drawing.Point(12, 351);
            this.buttonGo.Name = "buttonGo";
            this.buttonGo.Size = new System.Drawing.Size(75, 23);
            this.buttonGo.TabIndex = 3;
            this.buttonGo.Text = "Go";
            this.buttonGo.UseVisualStyleBackColor = true;
            this.buttonGo.Click += new System.EventHandler(this.buttonGo_Click);
            // 
            // progressBarFound
            // 
            this.progressBarFound.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBarFound.Location = new System.Drawing.Point(12, 210);
            this.progressBarFound.Name = "progressBarFound";
            this.progressBarFound.Size = new System.Drawing.Size(425, 23);
            this.progressBarFound.TabIndex = 4;
            // 
            // progressBarCopied
            // 
            this.progressBarCopied.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBarCopied.Location = new System.Drawing.Point(12, 261);
            this.progressBarCopied.Name = "progressBarCopied";
            this.progressBarCopied.Size = new System.Drawing.Size(425, 23);
            this.progressBarCopied.TabIndex = 5;
            // 
            // labelFound
            // 
            this.labelFound.AutoSize = true;
            this.labelFound.Location = new System.Drawing.Point(12, 193);
            this.labelFound.Name = "labelFound";
            this.labelFound.Size = new System.Drawing.Size(61, 13);
            this.labelFound.TabIndex = 6;
            this.labelFound.Text = "Files Found";
            // 
            // labelCopied
            // 
            this.labelCopied.AutoSize = true;
            this.labelCopied.Location = new System.Drawing.Point(12, 244);
            this.labelCopied.Name = "labelCopied";
            this.labelCopied.Size = new System.Drawing.Size(64, 13);
            this.labelCopied.TabIndex = 7;
            this.labelCopied.Text = "Files Copied";
            // 
            // labelKml
            // 
            this.labelKml.AutoSize = true;
            this.labelKml.Location = new System.Drawing.Point(12, 296);
            this.labelKml.Name = "labelKml";
            this.labelKml.Size = new System.Drawing.Size(78, 13);
            this.labelKml.TabIndex = 9;
            this.labelKml.Text = "Generating kml";
            // 
            // progressBarKml
            // 
            this.progressBarKml.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBarKml.Location = new System.Drawing.Point(12, 313);
            this.progressBarKml.Name = "progressBarKml";
            this.progressBarKml.Size = new System.Drawing.Size(425, 23);
            this.progressBarKml.TabIndex = 8;
            // 
            // PhotoSorterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(449, 387);
            this.Controls.Add(this.labelKml);
            this.Controls.Add(this.progressBarKml);
            this.Controls.Add(this.labelCopied);
            this.Controls.Add(this.labelFound);
            this.Controls.Add(this.progressBarCopied);
            this.Controls.Add(this.progressBarFound);
            this.Controls.Add(this.buttonGo);
            this.Controls.Add(this.groupBoxOut);
            this.Controls.Add(this.groupBoxIn);
            this.Name = "PhotoSorterForm";
            this.Text = "Photo Sorter";
            this.groupBoxIn.ResumeLayout(false);
            this.groupBoxIn.PerformLayout();
            this.groupBoxOut.ResumeLayout(false);
            this.groupBoxOut.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialogIn;
        private System.Windows.Forms.Button buttonIn;
        private System.Windows.Forms.GroupBox groupBoxIn;
        private System.Windows.Forms.TextBox textBoxIn;
        private System.Windows.Forms.GroupBox groupBoxOut;
        private System.Windows.Forms.TextBox textBoxOut;
        private System.Windows.Forms.Button buttonOut;
        private System.Windows.Forms.Button buttonGo;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialogOut;
        private System.Windows.Forms.ProgressBar progressBarFound;
        private System.Windows.Forms.ProgressBar progressBarCopied;
        private System.Windows.Forms.Label labelFound;
        private System.Windows.Forms.Label labelCopied;
        private System.Windows.Forms.Label labelKml;
        private System.Windows.Forms.ProgressBar progressBarKml;
    }
}

