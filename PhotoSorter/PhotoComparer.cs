﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PhotoSorter
{
    class PhotoComparer : IEqualityComparer<Photo>
    {
        #region IEqualityComparer<Photo> Members
        public bool Equals(Photo x, Photo y)
        {
            return x.Checksum == y.Checksum;
        }

        public int GetHashCode(Photo obj)
        {
            return obj.Checksum.GetHashCode();
        }
        #endregion
    }
}
