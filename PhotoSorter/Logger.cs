﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace PhotoSorter
{
    class Logger
    {
        private static Logger _instance = null;
        private string _logFilePath;

        public static Logger Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new Logger();
                return _instance;
            }
        }

        private Logger()
        {
            _logFilePath = System.Windows.Forms.Application.StartupPath;
            _logFilePath = Path.Combine(_logFilePath, "Log.txt");
        }

        public void Log(string data, string group = "info")
        {
            Log((object)data, group);
        }

        public void Log(object data, string group = "info")
        {
            lock (_instance)
            {
                group = group.ToUpper();
                var log = string.Format("{0} - {1} - {2}{3}", DateTime.Now, group, data, Environment.NewLine);
                File.AppendAllText(_logFilePath, log);
            }
        }

        public void Log<T>(IEnumerable<T> data, string group = "info")
        {
            group = group.ToUpper();
            var log = string.Format("{0} - {1}", DateTime.Now, group);

            lock (_instance)
            {
                using (var file = File.AppendText(_logFilePath))
                {
                    file.WriteLine(string.Format("{0}", log));
                    foreach (var item in data)
                        file.WriteLine(string.Format("\t{0}", item));
                }
            }
        }
    }
}
