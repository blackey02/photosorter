﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PhotoSorter
{
    class Location
    {
        public string City { set; get; }
        public string State { get; set; }
        public string Country { get; set; }

        public Location()
        { }

        public Location(string location)
        {
            var splitLocation = location.Split(',');
            City = splitLocation[0].Trim();
            State = splitLocation[1].Trim();
            Country = splitLocation[2].Trim();
        }

        public string DirectoryFriendly
        {
            get
            {
                var friendlyName = string.Format("{0}_{1}_{2}", City, State, Country).Trim();
                friendlyName = friendlyName.Replace(" ", "-");
                return friendlyName;
            }
        }

        public override string ToString()
        {
            return string.Format("{0}, {1}, {2}", City, State, Country).Trim();
        }
    }
}
