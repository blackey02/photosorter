﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using System.Threading;

namespace PhotoSorter
{
    class Mapper
    {
        public event Action<object, float, string> PhotoProcessedEvent;
        private List<Photo> _photos;
        private List<XDocument> _docs = new List<XDocument>();

        public Mapper(List<Photo> photos)
        {
            _photos = photos;
        }

        private void FirePhotoProcessedEvent(float pct, string file)
        {
            if (PhotoProcessedEvent != null)
                PhotoProcessedEvent(this, pct, file);
        }

        public void GenerateMaps(string savePath)
        {
            string basicXml = "" +
                "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                "<kml xmlns=\"http://earth.google.com/kml/2.2\">" +
                "<Document>" +
                "  <name>Where I&#39;ve Been</name>" +
                "  <description><![CDATA[]]></description>" +
                "  <Style id=\"style1\">" +
                "    <IconStyle>" +
                "      <Icon>" +
                "        <href>http://maps.gstatic.com/mapfiles/ms2/micons/blue-dot.png</href>" +
                "      </Icon>" +
                "    </IconStyle>" +
                "  </Style>" +
                "</Document>" +
                "</kml>";

            int splitCount = Properties.Settings.Default.KmlSplitCount;
            bool splitKml = splitCount != 0;
            string importPath = Properties.Settings.Default.ExistingKmlPath;
            bool cityDistinct = Properties.Settings.Default.KmlCityDistinct;
            bool importKml = !splitKml && !string.IsNullOrWhiteSpace(importPath);
            var citiesFound = new List<string>();

            XDocument doc = null;
            XNamespace ns = null;
            XElement style = null;
            List<XElement> placemarks = null;
            var SwitchDoc = new Action(() =>
                {
                    placemarks = new List<XElement>();
                    doc = importKml ? XDocument.Parse(File.ReadAllText(importPath)) : XDocument.Parse(basicXml);
                    doc.Declaration = new XDeclaration("1.0", "ascii", null);
                    ns = doc.Root.Name.Namespace;
                    style = doc.Descendants(ns + "Style").First();
                    _docs.Add(doc);
                });

            SwitchDoc();

            if (importKml && cityDistinct)
            {
                foreach (XElement elem in doc.Descendants(XName.Get("Placemark", ns.NamespaceName)))
                {
                    string cityFound = elem.Element(XName.Get("name", ns.NamespaceName)).Value;
                    var location = new Location(cityFound);
                    citiesFound.Add(location.DirectoryFriendly);
                }
            }

            int count = 0;
            int fileId = 0;
            foreach (var photo in _photos)
            {
                bool proceedTest = true;
                proceedTest = proceedTest && photo.ExifData.HasGps;

                if (cityDistinct)
                {
                    proceedTest = proceedTest && photo.ExifData.ExifLocation != null;
                    proceedTest = proceedTest && citiesFound.Find(directoryId => 
                        directoryId == photo.ExifData.ExifLocation.DirectoryFriendly) == null;
                }

                if (!proceedTest)
                {
                    FirePhotoProcessedEvent(Interlocked.Increment(ref count) * 1.0f / _photos.Count, photo.Path);
                    continue;
                }

                if (cityDistinct)
                    citiesFound.Add(photo.ExifData.ExifLocation.DirectoryFriendly);

                var placemark = new XElement(XName.Get("Placemark", ns.NamespaceName));
                var item = new XElement(XName.Get("name", ns.NamespaceName));
                item.Value = photo.ExifData.HasLocation ? photo.ExifData.ExifLocation.ToString() : "Been Here";
                placemark.Add(item);
                item = new XElement(XName.Get("description", ns.NamespaceName));
                item.Value = photo.ExifData.HasDate ? photo.ExifData.ExifDate.ToString("yyyy-MM-dd HH:mm:ss") : string.Empty;
                placemark.Add(item);
                item = new XElement(XName.Get("styleUrl", ns.NamespaceName));
                item.Value = "#style1";
                placemark.Add(item);
                item = new XElement(XName.Get("Point", ns.NamespaceName));
                item.Add(new XElement(XName.Get("coordinates", ns.NamespaceName)));
                ((XElement)item.FirstNode).Value = string.Format("{0},{1},0.000000", photo.ExifData.ExifLongitude, photo.ExifData.ExifLatitude);
                placemark.Add(item);
                placemarks.Add(placemark);

                if (splitKml && placemarks.Count == splitCount)
                {
                    style.AddAfterSelf(placemarks);
                    doc.Save(Path.Combine(savePath, string.Format("Data{0}.kml", ++fileId)));
                    SwitchDoc();
                }

                FirePhotoProcessedEvent(Interlocked.Increment(ref count) * 1.0f / _photos.Count, photo.Path);
            }

            if (placemarks.Count > 0)
            {
                style.AddAfterSelf(placemarks);
                doc.Save(Path.Combine(savePath, "Data.kml"));
            }
        }
    }
}
